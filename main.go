package main

import (
	"flag"
	"os"
	"runtime"

	"gitlab.com/otus_golang/gocopy/internal/gocopy"
)

const (
	exitCodeOk int = iota
	exitFatalError
)

func parseFlags(copier *gocopy.Copier) {
	flag.StringVar(&copier.From, "from", "", "copy from")
	flag.StringVar(&copier.To, "to", "", "copy to")
	flag.Int64Var(&copier.Offset, "offset", 0, "offset in input file")
	flag.Int64Var(&copier.Limit, "limit", 0, "limit to copy")

	flag.Parse()
}
func mainCmd() int {
	runtime.GOMAXPROCS(runtime.NumCPU())

	copier := gocopy.NewCopier()
	parseFlags(copier)

	_, err := copier.Copy()

	if err != nil {
		return exitFatalError
	}

	return exitCodeOk

}

func main() {
	os.Exit(mainCmd())
}
