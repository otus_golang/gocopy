package gocopy

import (
	"fmt"
	"io"
	"log"
	"os"
)

type Copier struct {
	From   string
	To     string
	Offset int64
	Limit  int64
}

func NewCopier() *Copier {
	c := Copier{}
	return &c
}

func (c *Copier) Copy() (int64, error) {
	var written int64
	var err error

	validateParams(c)

	source, err := os.Open(c.From)

	if err != nil {
		return 0, err
	}

	defer source.Close()

	_, err = source.Seek(c.Offset, io.SeekStart)

	if err != nil {
		return 0, err
	}

	dest, err := os.Create(c.To)

	if err != nil {
		return 0, err
	}
	defer dest.Close()

	if c.Limit == 0 {
		written, err = io.Copy(dest, source)
	} else {
		written, err = io.CopyN(dest, source, c.Limit)
	}

	if err == nil {
		fmt.Printf("Copying from %s to %s done\nWritten %d bytes\n\n", c.From, c.To, written)
	} else {
		fmt.Printf("Copying from %s to %s failed\nWritten %d bytes\nError %s\n\n", c.From, c.To, written, err)
	}
	return written, err
}

func validateParams(c *Copier) {
	if c.To == "" || c.From == "" {
		log.Panic("You must set From and To params")
	}

	sourceFileStat, err := os.Stat(c.From)
	if err != nil {
		panic(err)
	}

	if !sourceFileStat.Mode().IsRegular() {
		log.Panicf("%s is not a regular file", c.From)
	}
}

func getExpectedSize(c *Copier) int64 {
	sourceInfo, _ := os.Stat(c.From)

	if c.Limit == 0 {
		return sourceInfo.Size() - c.Offset
	} else {
		if c.Limit+c.Offset > sourceInfo.Size() {
			return sourceInfo.Size() - c.Offset
		} else {
			return c.Limit
		}
	}
}
