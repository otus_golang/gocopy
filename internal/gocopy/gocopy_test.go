package gocopy

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

const testDestination = "./testdata/test_generated_file.txt"
const testSource = "./testdata/dummy.txt"

var tests = []Copier{
	{
		testSource,
		testDestination,
		0,
		0,
	},
	{
		testSource,
		testDestination,
		0,
		10,
	},
	{
		testSource,
		testDestination,
		0,
		10,
	},
}

func TestCopy(t *testing.T) {
	defer os.Remove(testDestination)
	for _, copier := range tests {
		assert.True(t, !fileExists(testDestination))
		_, _ = copier.Copy()
		assert.FileExists(t, testDestination)

		destinationInfo, _ := os.Stat(testDestination)

		assert.Equal(t, getExpectedSize(&copier), destinationInfo.Size())
	}
}

func fileExists(path string) bool {
	_, err := os.Stat(path)
	return os.IsExist(err)
}
