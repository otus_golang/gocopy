test:
	go test gitlab.com/otus_golang/gocopy/internal/gocopy
fmt:
	gofmt -w -s -d .
vet:
	go vet .
lint:
	golint .
tidy:
	go mod tidy
verify:
	go mod verify
imports:
	goimports -w .
precommit: fmt vet lint tidy verify imports